"""Main app."""
# app.py

import os

from flask import Flask, jsonify, request, abort, send_from_directory
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Integer, String
from threading import Thread

from utils import get_versions, request as local_request
from docker_image import build_and_run
import settings


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = settings.DATABASE
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

db = SQLAlchemy(app)

BASE_DIR = os.path.dirname(os.path.abspath("__file__"))
SCRIPTS = os.path.join(BASE_DIR, "test_scripts")


class Test(db.Model):
    __tablename__ = 'test'

    id = db.Column(Integer, primary_key=True, autoincrement=True)
    status = db.Column(String(50))

    def __repr__(self):
        return '<Test %r, status: %r>' % (self.id, self.status)


def init_db():
    db.create_all()


def error_response(error_message=""):
    return jsonify({'error': error_message})


def response(content={}, status_code=None):
    if status_code and isinstance(status_code, int):
        return jsonify(content), status_code
    return jsonify(content)


def create_task(version):
    test = Test()
    db.session.add(test)
    db.session.commit()
    task = Thread(target=long_test, args=[test.id, version])
    task.start()
    return test.id


def update_status(task_id, status):
    try:
        t = Test.query.get(task_id)
        t.status = status
        db.session.commit()
    except Exception as e:
        print (e)
        raise
    print("Test status updated")


def long_test(test_id, version):
    build_and_run(test_id, version)


@app.route('/ubuntu/versions', methods=['GET'])
def get_ubuntu_versions():
    try:
        r = get_versions()
    except:
        return error_response("Can not obtain versions from Docker Hub")

    try:
        r.json()
    except:
        return error_response(
            "Can not parse Docker Hub response to JSON format")

    versions = []
    for item in r.json():
        versions.append(item['name'])

    return response({'versions': versions})


@app.route('/test/run', methods=['POST'])
def post_test_task():
    if not request.is_json or not request.get_json():
        abort(400)
    data = request.get_json()

    if 'version' not in data.keys():
        abort(400)

    versions = None
    try:
        res = local_request("GET", url=settings.LOCAL_URL + '/ubuntu/versions')
        versions = res.json()
        versions = (versions['versions'])
    except Exception as e:
        msg = "Can not obtain Ubuntu versions from Docker Hub.{0}".format(e)
        return error_response(msg)

    if versions is None or not isinstance(versions, list):
        msg = "Can not decode data from json"
        return error_response(msg)

    if data['version'] not in versions:
        msg = "Can not find version {0} in Docker Hub.".format(data['version'])
        return error_response(msg)

    test_id = create_task(data['version'])
    return response(dict(test_id=test_id), status_code=201)


@app.route('/test/status', methods=['GET'])
def test_status_all():
    """Get all tests with their ids and status."""
    data = []
    tests = Test.query.all()
    for test in tests:
        data.append({
            'test_id': test.id,
            'test_status': test.status})
    print("Getting tasks results")
    return response(data)


@app.route('/test/status/<int:test_id>', methods=['GET', 'POST'])
def test_status(test_id):
    """Get or set status for test with id=test_id."""
    data = []
    if request.method == 'POST':
        print("Setting task results")
        if not request.is_json or not request.get_json():
            abort(400)
        data = request.get_json()
        if data is None:
            abort(400)
        print("Settings task {0} with status with {1}".format(test_id, data))
        try:
            update_status(test_id, data['status'])
        except:
            abort(400)
    else:
        print("Getting task results")
        # print ("Thread count:", active_count())
        tests = Test.query.filter_by(id=test_id).all()
        for t in tests:
            data.append(dict(test_status=t.status))

    return response(data)


@app.route('/scripts/<path:filename>', methods=['GET'])
def get_script(filename):
    return send_from_directory(SCRIPTS, filename, as_attachment=True)
