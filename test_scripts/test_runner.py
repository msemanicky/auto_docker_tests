import sys
import unittest
from docker_test import Test


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(Test)
    result = unittest.result.TestResult()
    suite.run(result)
    print("test_results:")
    print(result.wasSuccessful())
    # unittest.main(verbosity=2)
