# manage.py
#

from flask_script import Manager
from app import app

from commands import (CreateDB, DropDB, DeleteAllRecords)


manager = Manager(app)
manager.add_command('create_database', CreateDB())
manager.add_command('drop_database', DropDB())
manager.add_command('delete_all_records', DeleteAllRecords())


if __name__ == "__main__":
    manager.run()
