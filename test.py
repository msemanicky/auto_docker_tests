import unittest
import requests
import json

from utils import generate_random_name, get_versions
from app import app, db, Test
from docker_image import build_and_run


TEST_UBUNTU_VERSION = "16.04"
TEST_SCRIPT = "docker_test.py"


class UtilsTest(unittest.TestCase):
    def test_len_random_name(self):
        STRING_LEN = 16
        self.assertEqual(len(generate_random_name()), STRING_LEN)

    def test_random_name(self):
        name1 = generate_random_name()
        name2 = generate_random_name()

        self.assertNotEqual(name1, name2)

    def test_get_versions(self):
        response = get_versions()

        self.assertIsNotNone(response)

    def test_get_versions_response(self):
        response = get_versions()

        self.assertTrue(isinstance(response, requests.Response))
        self.assertTrue(isinstance(response.json(), list))

    def test_get_versions_content(self):
        response = get_versions()

        self.assertGreater(len(response.json()), 5)


class DBTest(unittest.TestCase):
    def setUp(self):
        self.db_name = "test.db"

        app.testing = True

        app.config['SQLALCHEMY_DATABASE_URI'] = (
            'sqlite:///db/' + self.db_name)
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

        db.session.close()
        db.drop_all()
        db.create_all()

    def test_create(self):
        t = Test.query.all()

        self.assertEqual(t, [])

    def test_insert(self):
        t = Test()
        db.session.add(t)
        db.session.commit()

        obj = Test.query.all()
        self.assertEqual(len(obj), 1)

    def test_inserts(self):
        t = Test()
        db.session.add(t)
        db.session.commit()

        t = Test()
        db.session.add(t)
        db.session.commit()

        obj = Test.query.all()
        self.assertEqual(len(obj), 2)

    def test_status(self):
        status = 'success'
        t = Test(status=status)
        db.session.add(t)
        db.session.commit()
        res = Test.query.first()

        self.assertEqual(res.status, status)


class AppTest(unittest.TestCase):
    def setUp(self):

        self.db_name = "test.db"

        app.testing = True

        app.config['SQLALCHEMY_DATABASE_URI'] = (
            'sqlite:///db/' + self.db_name)
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

        self.client = app.test_client()

        db.session.close()
        db.drop_all()
        db.create_all()

    def test_home_has_404(self):
        rv = self.client.get('/')

        self.assertEqual(rv.status_code, 404)

    def test_get_ubuntu_versions(self):
        rv = self.client.get('/ubuntu/versions')

        self.assertEqual(rv.status_code, 200)
        self.assertIn(TEST_UBUNTU_VERSION, str(rv.data))

    def test_get_script_file(self):
        rv = self.client.get('/scripts/' + TEST_SCRIPT)

        self.assertEqual(rv.status_code, 200)
        self.assertIn("attachment", str(rv.headers))
        self.assertIn(TEST_SCRIPT, str(rv.headers))

    def test_get_script_file_bad_name_404(self):
        rv = self.client.get('/scripts/' + 'bad_name')

        self.assertEqual(rv.status_code, 404)

    def test_get_status_empty_db(self):
        rv = self.client.get('/test/status')

        self.assertEqual(rv.status_code, 200)
        self.assertIn('[]', str(rv.data))

    def test_get_status_all(self):
        status1 = "success"
        status2 = "failed"

        test = Test(status=status1)
        db.session.add(test)
        db.session.commit()
        test = Test(status=status2)
        db.session.add(test)
        db.session.commit()

        rv = self.client.get('/test/status')

        self.assertEqual(rv.status_code, 200)
        self.assertIn(status1, str(rv.data))
        self.assertIn(status2, str(rv.data))

    def test_get_test_status(self):
        status1 = "success"
        status2 = "failed"

        test = Test(status=status1)
        db.session.add(test)
        db.session.commit()
        test = Test(status=status2)
        db.session.add(test)
        db.session.commit()

        rv = self.client.get('/test/status/' + str(test.id))

        self.assertEqual(rv.status_code, 200)
        self.assertNotIn(status1, str(rv.data))
        self.assertIn(status2, str(rv.data))

    def test_set_status(self):
        status = "success"

        test = Test()
        db.session.add(test)
        db.session.commit()

        self.assertEqual(Test.query.first().status, None)

        data = dict(status=status)
        rv = self.client.post('/test/status/' + str(test.id),
                              data=json.dumps(data),
                              content_type='application/json')

        self.assertEqual(rv.status_code, 200)
        self.assertEqual(Test.query.first().status, status)

    def test_set_status_no_id(self):
        status = 'success'
        test = Test()
        db.session.add(test)
        db.session.commit()

        self.assertEqual(Test.query.first().status, None)

        data = dict(status=status)
        rv = self.client.post('/test/status/',
                              data=json.dumps(data),
                              content_type='application/json')

        self.assertEqual(rv.status_code, 404)

    def test_set_status_bad_id(self):
        status = 'success'
        bad_id = 123

        test = Test()
        db.session.add(test)
        db.session.commit()

        self.assertEqual(Test.query.first().status, None)

        data = dict(status=status)
        rv = self.client.post('/test/status/' + str(bad_id),
                              data=json.dumps(data),
                              content_type='application/json')

        self.assertEqual(rv.status_code, 400)

    def test_set_status_no_status(self):
        test = Test()
        db.session.add(test)
        db.session.commit()

        self.assertEqual(Test.query.first().status, None)

        rv = self.client.post('/test/status/' + str(test.id))

        self.assertEqual(rv.status_code, 400)


class DockerImageTest(unittest.TestCase):
    def setUp(self):

        self.db_name = "test.db"

        app.testing = True

        app.config['SQLALCHEMY_DATABASE_URI'] = (
            'sqlite:///db/' + self.db_name)
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

        self.client = app.test_client()

        db.session.close()
        db.drop_all()
        db.create_all()

    def test_run(self):
        data = dict(version="16.04")
        rv = self.client.post('/test/run',
                              data=json.dumps(data),
                              content_type='application/json')

        self.assertEqual(rv.status_code, 201)
        self.assertIn("test_id", str(rv.data))

    def test_run_bad_version(self):
        data = dict(version="bad_version")
        rv = self.client.post('/test/run',
                              data=json.dumps(data),
                              content_type='application/json')

        self.assertEqual(rv.status_code, 200)
        self.assertIn("Can not find version", str(rv.data))

    def test_run_no_version(self):
        rv = self.client.post('/test/run')

        self.assertEqual(rv.status_code, 400)


if __name__ == '__main__':
    unittest.main()
