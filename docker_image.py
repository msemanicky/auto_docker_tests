"""Build and run docker image."""

from io import BytesIO
import docker

from utils import request, generate_random_name
import settings


def build_and_run(test_id, version=""):
    # import time
    # time.sleep(4)
    cli = docker.APIClient(base_url='unix://var/run/docker.sock')

    exited = cli.containers(quiet=True, all=True, filters={'status': 'exited'})
    for container in exited:
        try:
            cli.remove_container(container['Id'])
        except Exception as e:
            print("Error during removing exited containers. {0}".format(e))

    dangling = cli.images(quiet=True, all=True, filters={'dangling': True})
    for image in dangling:
        try:
            cli.remove_image(image)
        except Exception as e:
            print("Error during removing images {0}".format(e))

    dockerfile = '''
    FROM ubuntu:{0}
    ADD http://127.0.0.1:5000/scripts/docker_test.py /docker_test.py
    ADD http://127.0.0.1:5000/scripts/test_runner.py /test_runner.py
    RUN apt-get update
    RUN apt-get install -y python
    RUN /usr/bin/python /test_runner.py
    '''.format(version)

    f = BytesIO(dockerfile.encode('utf-8'))

    image_name = generate_random_name()
    response = [line for line in cli.build(
        fileobj=f, rm=True, tag=image_name
    )]

    data = dict(status='Failed')

    for r in response:
        if b'test_results:\\nTrue' in r:
            data['status'] = "Success"
    try:
        url = settings.LOCAL_URL + "/test/status/" + str(test_id)
        response = request(method="POST", url=url, json=data)
        print (response.status_code)
    except Exception as e:
        print("Error during setting task status.{0}".format(e))
    finally:
        cli.remove_image(image_name, force=True)
