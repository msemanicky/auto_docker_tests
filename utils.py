import requests
import string
import random
# import urllib3
# urllib3.disable_warnings()

import settings


class Error(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


def generate_random_name():
    chars = string.digits + string.ascii_lowercase
    return "".join(random.choice(chars) for i in range(16))


def request(method="GET", url=settings.LOCAL_URL, data=None, params=None,
            auth=None, json=None, redirects=False,
            timeout=settings.REQUEST_TIMEOUT):

        response = None

        try:
            if method.upper() == "GET":
                response = requests.get(
                    url, data=data, params=params, auth=auth,
                    allow_redirects=redirects, timeout=timeout)

            elif method.upper() == "POST":
                response = requests.post(
                    url, data=data, json=json, params=params, auth=auth,
                    allow_redirects=redirects, timeout=timeout)
        except:
            raise

        return response


def get_versions():
    """Get available Ubuntu versions on Docker hub."""
    try:
        response = request("GET", url=settings.DOCKER_URL)
    except:
        raise

    if response.status_code != requests.codes.ok:
        raise Error("Bad response status code raised")

    return response
