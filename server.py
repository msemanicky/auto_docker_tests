"""REST API server to handle request to run python tests in Docker container."""
# server.py

import settings

from app import app, init_db


if __name__ == '__main__':
    init_db()
    app.run(
        settings.BIND_ADDRESS,
        settings.BIND_PORT,
        threaded=True,
        debug=settings.SERVER_DEBUG)
