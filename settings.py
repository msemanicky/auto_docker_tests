# Settings

BIND_ADDRESS = '127.0.0.1'
BIND_PORT = 5000
LOCAL_URL = "http://{0}:{1}".format(BIND_ADDRESS, BIND_PORT)
SERVER_DEBUG = True

DATABASE = 'sqlite:///' + 'db/sqlite3.db'

REQUEST_TIMEOUT = 5

DOCKER_URL = "https://registry.hub.docker.com//v1/repositories/ubuntu/tags"
