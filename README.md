# README #

### What is this repository for? ###

* REST API server to run automated python tests in various versions of Ubuntu in Docker container.  
Developed and tested in Python 3.5  

### How to run server ? ###
Default run on http://127.0.0.1:5000  

bash command in root directory:  
$ python server.py  


### How to run test ? ###
  
POST /test/run with version="16.04" in request data, create Docker container with image of Ubuntu:16.04 and start the tests.  
  
GET /test/status return history of all runned tests  
GET /test/status/<test_id> return status of test with id <test_id>. Status options for test = ["Success", "Failed"]  
  
GET /ubuntu/versions return all available versions of Ubuntu on the Docker Hub.  
