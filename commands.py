# commands.py
#

from flask_script import Command

from app import db, Test


class DropDB(Command):
    def run(self):
        db.drop_all()


class CreateDB(Command):
    def run(self):
        db.create_all()


class DeleteAllRecords(Command):
    def run(self):
        for record in Test.query.all():
            db.session.delete(record)
            db.session.commit()
