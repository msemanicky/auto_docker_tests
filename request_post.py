"""Run puthon tests in Docker Ubuntu."""

import settings
import time

from utils import request

data = {'version': '16.04'}

response = request(method="POST", url=settings.LOCAL_URL + "/test/run",
                   json=data)

print(response.status_code)
data = (response.json())
test_id = data['test_id']


while True:
    response = request(method="GET",
                       url=settings.LOCAL_URL + "/test/status/" + str(test_id))

    print(response.status_code)
    print(response.content)
    time.sleep(1)
